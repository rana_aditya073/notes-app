
// const name='Your notes...'
const fs=require('fs')
const chalk=require('chalk')

const getNotes= ()=>{
    return 'Your notes are...'
}

const addNote= (title, body)=>{
    const notes=loadNotes()

    // const duplicates=notes.filter((note)=>{
    //         return note.title===title
    // })

    const duplicatesnote=notes.find((note)=>note.title==title)


    if(!duplicatesnote){
        notes.push({
            title:title,
            body:body
        })
        saveNotes(notes)



        
        console.log(chalk.blue.inverse('New Note added'))
    }else{

        console.log(chalk.red.inverse('Note title taken!'))

    }   

    // console.log(notes)
}


const removeNote=(title)=>{
    const notes=loadNotes()

    const getTitle= notes.filter((notes)=>{
        return notes.title!==title
    })
    // console.log(notes)
// console.log(getTitle)
    if(notes.length>getTitle.length){
    saveNotes(getTitle)
    return true
    }else{
        return false
    }
}

const ListNotes= ()=>{
        const notes=loadNotes()

        notes.forEach((note)=>{
            console.log(note.title+'->'+note.body)
        })
}

const ReadNotes=(title)=>{
    const notes=loadNotes()

    const getTitle=notes.find((notes)=>notes.title===title)

    if(getTitle){
        notes.forEach((note)=>{
            console.log(chalk.inverse(note.title)+'->'+note.body)
        })
    }
    else{
        console.log(chalk.red.inverse('No note found'))
    }

}

const saveNotes= (notes)=>{
const dataJSON=JSON.stringify(notes)
fs.writeFileSync('notes.json',dataJSON)
}


const loadNotes=()=>{

    try{
        const databuffer=fs.readFileSync('notes.json')
        const dataJSON=databuffer.toString()
        return JSON.parse(dataJSON)
    }
    catch(e){
        return []
    }

    
}

module.exports= {
    getNotes:getNotes,
    addNote: addNote,
    removeNote:removeNote,
    ListNotes:ListNotes,
    ReadNotes:ReadNotes
    
}
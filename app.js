const chalk= require('chalk')
const { demand, argv } = require('yargs')
const yargs=require('yargs')
const notes=require('./notes.js')

//18








//15 + 16 + 17
yargs.version('1.1.0')

//add command
yargs.command({
    command:'add',
    describe:'Add a new Note', // take care of commas :P
    builder:{
        title:{
            describe:'Note title',
            demandOption: true ,  // false by default
            type:'string'
        },
        body:{
            describe:'Note body',
            demandOption: true,
            type: 'string'

        }
    },
    handler:function(argv){
        // console.log('Add a new Note',argv)
        // console.log('Title:'+ argv.title)
        // console.log('Body:'+ argv.body)

        notes.addNote(argv.title, argv.body)
        
    }
})

// remove command

yargs.command({
    command:'remove',
    describe:'Remove a new Note',
    builder:{
        title:{
            describe:'Note title',
            demandOption: true ,  // false by default
            type:'string'
        }
    },     
    handler:function(){
       const getTitle= notes.removeNote(argv.title)

       if(getTitle){
           console.log(chalk.green.inverse('Note Removed'))
       }else{
        console.log(chalk.red.inverse('No Note found'))
       }
        
    }
})

// list command
yargs.command({

command:'list',
describe:'List your Note',
handler:function(){
    console.log(chalk.grey.inverse('Listing out all notes'))
    notes.ListNotes()
}
})

// read command

yargs.command({

    command:'read',
    describe:'Reading your Note',
    builder:{
        title:{
            describe:'Note title',
            demandOption: true ,  // false by default
            type:'string'
        }
    },
    handler:function(){
        // console.log('Reading a Note')
        notes.ReadNotes(argv.title)
    }
    })

yargs.parse()
// console.log(yargs.argv)








//const command= process.argv[2]  // argv is argument vector

// if(command==='add'){
// console.log('Success!')
// }else{
//     console.log('Whoops!!')
// }


// 13 intro to nodemon


// 12
// const chalk = require('chalk')
// const msg= chalk.red.bold('Hello World')
// console.log(msg);

// to stop nodemon hit Ctrl+C

// 11
// const validator= require('validator')
// const str= require('./notes.js')
// const name=str()
// console.log(name)
// console.log(validator.isURL('https://www.npmjs.com//validator'))

// 10
// const add = require('./utils')
// const sum=add(3,4)
// console.log(sum)
